# Running your Python Bot

So you followed the example, think you know what you're doing now?

<<<<<<< HEAD
If your bot is simple you should be able to just copy the Dockerfile into your bots code and check your requirements.txt to ensure you are not using git links for any dependencies otherwise you will need to install git inside the container, you may also need to edit the CMD line to point to your main bot file, you should know what needs to go here though. But if you bot is a little more complicated and needs c based modules you will get errors like:
=======
If your bot is simple you should be able to just copy the Dockerfile into your bots code and check your requirements.txt to ensure you are using the zip link not the git link, may also need to edit the CMD line to point to your main bot file, you should know what needs to go here though. But if your bot is a little more complicated and needs C based modules you will get errors like:
>>>>>>> 6a6b9e5e3e706bf379bfb8ab30da630ea011da22

![compile error](./imgs/9.png)

Time to install some build tools right? Nope!

We will instead install this from `apk add` but how do you know what to install?
```
docker run -it --rm alpine
```
<<<<<<< HEAD
This will spin up a temporary alpine container for our searching.
=======
This will spin up a temporary Alpine container for our searching.
>>>>>>> 6a6b9e5e3e706bf379bfb8ab30da630ea011da22
```
apk update
```
Will make sure that your Alpine container knows about any new packages.
```
apk search pynacl
```
![pynacl](./imgs/10.png)

<<<<<<< HEAD
Im using pynacl as an example because it is the module that discord.py needs to support voice, that `py3-pynacl` seems like the right package. Now time to change your docker file to look like this:
=======
I'm using PyNaCL as an example because it is the module that discord.py needs to support voice, that `py3-pynacl` seems like the right package. Now time to change your Dockerfile to look like this:
>>>>>>> 6a6b9e5e3e706bf379bfb8ab30da630ea011da22
```Dockerfile
FROM alpine
ADD . /src
WORKDIR /src
RUN apk add --no-cache python3 py3-pynacl; pip3 install -r requirements.txt
CMD python3 bot.py
```
Notice how I don't need to do `apk update` inside the Dockerfile, that is because the `--no-cache` will always check the Internet when searching for packages. You can now safely type `exit` in that container now and it will then be gone forever. Just needed it for a quick search.
```
docker build .
```
And now it should build. You can have all that musical goodness! Well you would still need to install Opus and FFmpeg but they are available from `apk add` in the same way.
