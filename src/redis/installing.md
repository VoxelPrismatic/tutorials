# Installing Redis

To install Redis you can either try to find an up-to-date version in your system's repositories (for Windows users you can try using [**Chocolatey**](https://chocolatey.org/)) or you can build it from source. That's what I'll be explaining on this section. You can skip this if you were able to install a recent version using your package manager.

The [Redis Quick Start](https://redis.io/topics/quickstart) guide is very good but I will sum it up for you.  
You'll have to execute these commands in the shell one by one:
```bash
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
# This will get the correct number of your logical CPU cores
LC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || echo 4)
make -j${LC}
```
After completing all those tasks, you can do `make test -j${LC}` and then `sudo make install -j${LC}` to put the binaries in your PATH (basically it allows you to execute `redis-server` and others whitout specifying the full path).  
Alternatively, you can copy the binaries manually or using the following commands 
```bash
sudo cp src/redis-server /usr/bin/redis-server
sudo cp src/redis-cli /usr/bin/redis-cli
```
*(these will only copy the server and the client binaries of course)*


**Note:** If you are using Docker to manage your app, the `redis:alpine` image is the best in the majority of cases. It's just [Alpine Linux](https://alpinelinux.org/) with Redis installed, so it's super lightweight.

**We are all done in this section! Let's start that Redis server up, just follow me to [Setting up the server](https://dcacademy.gitlab.io/dca-tuts/redis/setting-up-the-server.html)!

