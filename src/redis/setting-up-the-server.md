# Setting up the server

Now that you got Redis all ready to go, you wil need to start a server that will enable you to make operations with your data. 

So to start your redis server you can simply do `redis-server`. This will create a redis server with the default configurations.
However we'll want to tweak some things to protect our server and make the data persist.

For that we'll need a config file that will be named `redis.conf`. There's [**this**](http://download.redis.io/redis-stable/redis.conf) neat page that you can check out to learn everything about redis configuration.

Now, the configs I will focus on are `bind` (and `port`), `requirepass`, `append-only` (and `appendfilename`, `appendfsync`), `loglevel`, `logfile` and `save` (and `dbfilename`).

Let's get right into it:

- `bind` - This binds the Redis server to only listen to that address. Defaults to `127.0.0.1` which is the same as `localhost`, and that means that Redis will only listen to connections that come from the host machine. This is great if you run your clients and server in the same computer as it provides greater speeds and security. __NOTE that this may expose your Redis server to the *open* Internet! *(keep `protected-mode` on at all times)* __Also note__ that with Docker this won't work between containers, however, if you put the server container and the client(s) container(s) in the same network you can safely bind redis to `0.0.0.0` (services are run in the same network by default with Docker Compose). You can also use `port` to specify the port to which the server should listen to (defaults to `6379`).

- `requirepass` - This is security option that should be used if you are going to *"expose"* your server to the Internet. Since a lot of passwords can be tested per second, I suggest you get a strong password if you need this, and using a generator is probably an excellent idea (I suggest [Bitwarden](https://bitwarden.com/)'s generator by experience)

- `append-only` - AppendOnlyFile is one of Redis' data persistence systems. It basically consists on appending every operation done in the instance to a log file, so that the data you lose in case of a disaster like power outage, accidentally killing the process or other bad stuff. I only recommend this if you really need that persistency security, i.e. if you are doing lots of operations per minute or per second. To turn this feature on just add `appendonly yes` to your config file. There are three modes to AOF time windows - `no`, `everysec` and `always`. The first one will just let the OS deal with flushing the data which is generally around 30s or so; the second one will flush the buffer every second; the last one will flush it every time a new command come through. You can also change the appendonly filename with the `appendfilename` option.

- `save` - Saving is the main way of making your Redis data persist. It just snapshots your server and asynchronously saves the data to a file. You can adjust the timings on the saves with this config line. Note that this config takes two parameters - `seconds` and `changes` (signature: `save <seconds> <changes>`) - so it saves on the `seconds` period if enough changes are made, and the best thing is that you can have multiple lines of this config and it will register all the cases. You can also change the snapshot dump filename with the `dbfilename` option.

- `loglevel`, `logfile` - Pretty straightforward, it's the logger level and the log filename if wanted.

So, there they are, the basic and most important options for you to know. Either way, I suggest you take a look at the example file I linked above.

Here's an example of a simple config file that takes all those options into account:
```
# Leaving bind and port as the default configs
# Security
requirepass asdsmoviesarecool

# Logging
loglevel verbose
logfile redis.log

# Persistence
save 200 50
save 10 200

appendonly yes
```

To start the server taking in account the config file you just pass the path in the first argument, like this `redis-server redis.conf`.

**That's all for this section! If you wish to know how to use `aioredis` to incorporate Redis functionality into your Python app just continue to [Starting with `aioredis`](https://dcacademy.gitlab.io/dca-tuts/redis/starting-with-aioredis.html)**!
